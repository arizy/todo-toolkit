import Main from "./components/Todos/Main";

function App() {
  return (
    <div className="App">
      <Main />
    </div>
  );
}

export default App;
