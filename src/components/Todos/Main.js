import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { TODOS_FEATURE_KEY, addTodo, loadTodos, selectTodosList } from '../../store/todos.slice'

const Main = () => {
  const dispatch = useDispatch()
  // const todos = useSelector(state => state[TODOS_FEATURE_KEY])
  // const todos = useSelector(state => state[TODOS_FEATURE_KEY].entities) // 拿entities对象，因为我们用了适配器
  const todos = useSelector(selectTodosList)
  // console.log(todos) // {entities: {1:{id:1,title:'hh'},2:{id:2,title:'qq'} }, ids: [1,2] }

  useEffect(() => {
    // 调用loadTodos来触发action loadTodos的返回的是一个actionCreator函数
    // dispatch(loadTodos("http://localhost:3001/todos")) // 我这里优化了 请求地址不应该写在组件内，应该封装在一个方法里
    dispatch(loadTodos())
  }, [dispatch])

  return (
    <div>
      {/* 调用dispatch的时候需要传递action对象 */}
      {/* 而action对象是由actionCreator返回的 */}
      {/* 调用addTodo时需要传递一条任务，即一个对象 */}
      <button onClick={() =>
        // dispatch(addTodo({ id: Math.random(),title: "测试任务" }))}
        dispatch(addTodo({ title: "测试任务" }))}
      >添加任务</button>
      <ul>
        {
          todos.map(todo => {
          // Object.values(todos).map(todo => {
            return (
              <li key={todo.id}>
                <div className='view'>
                  <input type='checkbox' />
                  <label>{todo.title}</label>
                  {/* <button className='destroy'></button> */}
                </div>
                <input className='edit' />
              </li>
            )
          })
        }
      </ul>
    </div>
  )
}

export default Main