// import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit'
import { configureStore } from '@reduxjs/toolkit'
import TodosReducer, { TODOS_FEATURE_KEY } from './todos.slice'
import logger from 'redux-logger'

// configureStore 就是创建store
// configureStore方法的返回值就是我们需要的store，要把这个store配置给provider组件
export default configureStore({
  // 一个个小的reducer，之前我们用combineReducers去合并reducer
  // 现在是通过一个配置对象来做到这样的事情 实际上就是合并reducer
  reducer: {
    // 是ES6语法：当对象中的属性为变量的时候，需要在外面加一个中括号啦
    [TODOS_FEATURE_KEY]: TodosReducer
  },
  // 是否开启调试工具
  devTools: process.env.NODE_ENV !== "production",

  // 把内置的中间件先拿过来，再添加我们自己的中间件（下面这个被deprecated了，不需要引入了）
  // middleware: [...getDefaultMiddleware(), logger]
  // 该回调将 getDefaultMiddleware 作为其参数给出
  middleware: (getDefaultMiddleware) => [...getDefaultMiddleware(), logger]
})
