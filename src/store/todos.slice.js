import { createSlice, createAsyncThunk, createEntityAdapter, createSelector } from '@reduxjs/toolkit'
import { getTodos } from '../api'

// const todosAdapter = createEntityAdapter()
// 创建实体适配器，可以理解为是一个容器
// 实体适配器会给我们提供操作状态的方法，让我们简化对状态的增删改查的操作。
const todosAdapter = createEntityAdapter({
  selectId: todo => todo.id // 指定唯一标识是什么属性(默认是ID其实不用写 是其他就得写)
})

console.log(todosAdapter.getInitialState())
// { ids:[], entities:{} } 这样的数据结构叫字典

export const TODOS_FEATURE_KEY = "todos"

// createAsyncThunk可以用来执行异步操作，它的返回值就是触发执行异步操作的那个actionCreator函数
// 用来创建 用于执行异步操作的actionCreator函数
// export const loadTodos = createAsyncThunk("todos/loadTodos", (payload, thunkAPI) => {
//   axios.get(payload).then(res => thunkAPI.dispatch(setTodos(res.data))) // 这里调dispatch是因为获取数据后触发同步action
// })
// 改进一下：不再需要thunkAPI了
export const loadTodos = createAsyncThunk("todos/loadTodos", async (payload) => {
  // 请求地址来自 你在调用这个actionCreator函数时给我传进来 返回这个promise
  // return axios.get(payload).then(res => res.data)
  const {data} = await getTodos() // 这个案例里组件调接口不传参~
  return data
})

// 起别名，将来在这个应用中肯定有很多切片，每个切片返回的reducer函数都不能叫reducer，会冲突
const { reducer: TodosReducer, actions } = createSlice({
  // 配置状态切片
  name: TODOS_FEATURE_KEY, // 状态切片的唯一标识。slice切片指定名字,用来自动生成action中的type前缀
  // initialState: [],
  initialState: todosAdapter.getInitialState(), // todo任务的初始值
  reducers: {
    // 配置一个个小的reducer 实际上就是Switch case
    // 直接操作state就可以了
    // addTodo(state, action) {
    //   state.push(action.payload)
    // }

    // 要先预处理 所以它改成对象形式
    addTodo: {
      // 还可以对适配器操作进行简化
      // 原来reducer函数接收state和action作为参数，会把这俩参数给到addOne这个方法。
      // addOne这个方法内部会检测你传的第二个参数是否是action，是的话就会把action.payload放到第一个参数，即放到state里面
      // reducer: (state, action) => {
      //   console.log("action", action)
      //   // addOne 向实体适配器里面添加一条数据
      //   // state.push(action.payload)
      //   todosAdapter.addOne(state, action.payload)
      // },
      reducer: todosAdapter.addOne,
      // prepare是对action进行预处理的
      prepare: todo => {
        console.log("todo 会先输出", todo)
        // 这个参数就是在组件中触发action时传递的参数
        return {
          payload: { id: Math.random(), ...todo }
        }
      }
    },
    // setTodos: (state, action) => {
    //   // state就是上面那个数组，这里拿到的action.payload就是res.data
    //   // action.payload.forEach(todo => state.push(todo))
    //   todosAdapter.addMany(state, action.payload) // 内部会对这个数据进行遍历
    // }
    setTodos: todosAdapter.addMany // 下面 [loadTodos.fulfilled]: todosAdapter.addMany我就不替换了
  },

  //#region 
  /*
  // 要接收异步action
  // extraReducers传的对象依然是一个个reducer函数，上面reducers接收的是同步的action，而extraReducers里面接收的是异步的action
  extraReducers: {
    // loaderTodos返回的是promise，在接收action的时候可以去.promise的状态
    // 当这个异步操作执行成功的时候，执行其对应的函数。如果想监听请求状态，就是loaderTodos.pending，如果请求失败就去接收oaderTodos.rejected
    [loadTodos.fulfilled]: (state, action) => {
      console.log('fulfilled')
      // action.payload.forEach(todo => state.push(todo))
      todosAdapter.addMany(state, action.payload)
    },
    // 还可以去监听其他状态，如果它处于请求的过程中，可以执行函数
    [loadTodos.pending]: (state, action) => {
      // 你可以在这里写加载状态的东西
      console.log('pending')
      return state // 返回state 不做任何变化
    }
  },
  */
  //#endregion

  // The object notation for `createSlice.extraReducers` is deprecated, and will be removed in RTK 2.0. Please use the 'builder callback' notation instead
  // 上述写法 createSlice.extraReducers 已被移除，改进一下！
  extraReducers: builder => {
    builder.addCase(loadTodos.fulfilled, (state, action) => {
      // console.log(state,action)
      todosAdapter.addMany(state, action.payload)
      // 你的初始值是字典类型，那你操作数据也用适配器对应的方法比较好，不会出错
    })
  }
})

// 从实体适配器中所有的实体，并把所有的实体放到一个数组中进行返回
const { selectAll } = todosAdapter.getSelectors()

// 导出状态选择器
export const selectTodosList = createSelector(state => state[TODOS_FEATURE_KEY], selectAll)
// createSelector第一个参数：store里的对应state
// 第二个参数传的是一个选择器。实体适配器会给我们返回一些获取状态的方法

export const { addTodo, setTodos } = actions // 需要结构给上面使用（同步操作）
export default TodosReducer // 用于合并一个完整的reducer


/**
 * 是否用toolkit？嗯还是想用的。
 * 是否用实体适配器？额感觉可以不用（实体适配器会给我们提供操作状态的方法，让我们简化对状态的增删改查的操作。）
 * 是否要用状态选择器。感觉也不用。。。
 */


